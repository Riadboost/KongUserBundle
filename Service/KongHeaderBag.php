<?php
/** Namespace */
namespace Parkingmap\KongUserBundle\Service;

/** Usages */
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class KongHeaderBag
 * @package Parkingmap\KongUserBundleUser\Service
 */
class KongHeaderBag
{
    const HEADER_GROUPS                 = "x-consumer-groups";
    const HEADER_CREDENTIAL_USERNAME    = "x-credential-username";
    const HEADER_CONSUMER_USERNAME      = "x-consumer-username";
    const HEADER_CONSUMER_CUSTOM_ID     = "x-consumer-custom-id";
    const HEADER_CONSUMER_ID            = "x-consumer-id";

    const GROUP_ROLE                    = "role:";
    const GROUP_PARKGROUP               = "group:";
    const GROUP_PARK                    = "area:";

    /**
     * @var Request
     */
    private $request;

    /**
     * KongApi constructor.
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getMasterRequest();
    }

    /**
     * @param string $header
     * @return string|null
     */
    public function get(string $header): ?string
    {
        if (!$this->request) {
            return null;
        }
        
        return $this->request->headers->get($header, null);
    }

    /**
     * @return array
     */
    public function getKongGroups(): array
    {
        return array_map(
            'trim',
            explode(',', $this->get(self::HEADER_GROUPS))
        );
    }

    /**
     * @return array
     */
    public function getUserRoles(): array
    {
        return $this->filterKongGroupsByKey(self::GROUP_ROLE);
    }

    /**
     * @return array
     */
    public function getParkGroups(): array
    {
        return $this->filterKongGroupsByKey(self::GROUP_PARKGROUP);
    }

    /**
     * @return array
     */
    public function getParks(): array
    {
        return $this->filterKongGroupsByKey(self::GROUP_PARK);
    }

    /**
     * @param string $key
     * @return array
     */
    private function filterKongGroupsByKey($key): array
    {
        $filteredGroups = [];

        foreach ($this->getKongGroups() as $group) {
            if (0 === strpos($group, $key)) {
                list(,$value) = explode($key, $group);
                $filteredGroups[] = $value;
            }
        }

        return $filteredGroups;
    }
}
